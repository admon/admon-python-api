import time
from typing import Union, List, Dict, Any
import logging
from admonapi.admon_service import ServiceUtil
from admonapi import Project
from admonapi.model import ADMONObject, InputDataConfig, DetectionTaskState
from admonapi.admon_service.utils import guarantee_milliseconds


class DetectionEntity(ADMONObject):
    """
    A configuration entity to define an anomaly detection process.

    It can be created using various functions:
        .get() # Gives you a single `DetectionEntity`
        .get_many() # Gives a list of `DetectionEntity`
        .create() # Creates a new `DetectionEntity` in the admon-service and gives you the newly created entity
    """
    class_name = "Detection Entity"
    _base_path = "detectionentities/"

    def __init__(
        self,
        detection_entity_id: id,
        project: Union[Project, int],
        title: str,
        interval_minutes: int,
        sliding_interval_minutes: int,
        input_data_config: InputDataConfig,
        inference_model: str,
        inference_namespace: str,
        monit_producer: str,
        monit_label: Dict[str, str],
        processing_start_timestamp: int = None,
        _depth: int = 0
    ):
        """
        note: A DataFrame should only be created via the `.get()`, `.get_many()` or `create()` methods.
        It should not be directly created via using the constructor.
        """
        super().__init__(_depth)
        self._detection_entity_id = detection_entity_id
        self._project_id = project if isinstance(project, int) else project.id
        self._title = title
        self._interval_minutes = interval_minutes
        self._sliding_interval_minutes = sliding_interval_minutes
        self._input_data_config = input_data_config.with_depth(_depth + 1)
        self._inference_model = inference_model
        self._inference_namespace = inference_namespace
        self._monit_producer = monit_producer
        self._monit_label = monit_label
        self._processing_start_timestamp = processing_start_timestamp

    @property
    def id(self) -> int:
        return self._detection_entity_id

    @property
    def project_id(self) -> int:
        return self._project_id

    @property
    def title(self) -> str:
        return self._title

    @property
    def interval_minutes(self) -> int:
        return self._interval_minutes

    @property
    def sliding_interval_minutes(self) -> int:
        return self._sliding_interval_minutes

    @property
    def input_data_config(self) -> InputDataConfig:
        return self._input_data_config

    @property
    def inference_model(self) -> str:
        return self._inference_model

    @property
    def inference_namespace(self) -> str:
        return self._inference_namespace

    @property
    def monit_producer(self) -> str:
        return self._monit_producer

    @property
    def monit_label(self) -> Dict[str, str]:
        return self._monit_label

    @property
    def processing_start_timestamp(self) -> int:
        return self._processing_start_timestamp

    @staticmethod
    def get(title: str = None, detection_entity_id: int = None) -> "DetectionEntity":
        detection_entities = DetectionEntity.get_many(title, detection_entity_id)

        if len(detection_entities) != 1:
            if len(detection_entities) > 1:
                raise Exception("DetectionEntity with given attributes was ambiguous.")
            else:
                raise Exception("No DetectionEntity found with given attributes.")
        detection_entity = detection_entities[0]
        return detection_entity

    @staticmethod
    def get_many(title: str = None, detection_entity_id: int = None) -> List["DetectionEntity"]:
        path = DetectionEntity._base_path

        if detection_entity_id is not None:
            path += "/" + str(detection_entity_id)
        if title is not None:
            path += "?title=" + title

        de_jsons = ServiceUtil.make_get_request(
            path=path
        )
        detection_entities = [DetectionEntity.from_json(de_json) for de_json in de_jsons]

        logging.info(f"Get DetectionEntities: \n{detection_entities}")
        return detection_entities

    @staticmethod
    def create(
        project: Union[Project, int],
        title: str,
        interval_minutes: int,
        input_data_config: InputDataConfig,
        inference_model: str,
        inference_namespace: str,
        monit_producer: str,
        monit_label: Dict[str, str],
        sliding_interval_minutes: int = None,
    ) -> "DetectionEntity":
        to_be_created_de = DetectionEntity(
            detection_entity_id=0,
            project=project,
            title=title,
            interval_minutes=interval_minutes,
            sliding_interval_minutes=sliding_interval_minutes,
            input_data_config=input_data_config,
            inference_model=inference_model,
            inference_namespace=inference_namespace,
            monit_producer=monit_producer,
            monit_label=monit_label
        )
        if DetectionEntity.validate(to_be_created_de):
            de_json = ServiceUtil.make_post_request(
                path=DetectionEntity._base_path,
                body=to_be_created_de.to_json()
            )[0]

            detection_entity = DetectionEntity.from_json(de_json)
            logging.info(f"Created DetectionEntity: \n{detection_entity}")
            return detection_entity
        else:
            logging.info("Could not create DetectionEntity.")
            return None

    def update(
        self,
        project: Union[Project, int] = None,
        title: str = None,
        interval_minutes: int = None,
        sliding_interval_minutes: int = None,
        input_data_config: InputDataConfig = None,
        inference_model: str = None,
        inference_namespace: str = None,
        monit_producer: str = None,
        monit_label: Dict[str, str] = None,
        processing_start_timestamp: int = None
    ) -> None:
        to_be_created_de = DetectionEntity(
            detection_entity_id=self.id,
            project=project or self.project_id,
            title=title or self.title,
            interval_minutes=interval_minutes or self.interval_minutes,
            sliding_interval_minutes=sliding_interval_minutes or self.sliding_interval_minutes,
            input_data_config=input_data_config or self.input_data_config,
            inference_model=inference_model or self.inference_model,
            inference_namespace=inference_namespace or self.inference_namespace,
            monit_producer=monit_producer or self.monit_producer,
            monit_label=monit_label or self.monit_label,
            processing_start_timestamp=processing_start_timestamp or self.processing_start_timestamp
        )

        if DetectionEntity.validate(to_be_created_de):
            de_json = ServiceUtil.make_put_request(
                path=DetectionEntity._base_path + str(self.id),
                body=to_be_created_de.to_json()
            )[0]

            detection_entity = DetectionEntity.from_json(de_json)
            self.__dict__ = detection_entity.__dict__
            logging.info(f"Updated DetectionEntity to: \n{self}")
        else:
            logging.info("Could not update DetectionEntity.")

    def delete(self) -> None:
        ServiceUtil.make_delete_request(
            path=DetectionEntity._base_path + str(self.id)
        )
        logging.info(f"Deleted DetectionEntity: \n{self}")

    def start(self, at_timestamp=None) -> None:
        self.update(processing_start_timestamp=at_timestamp or int(time.time()))

    def stop(self) -> None:
        self._processing_start_timestamp = None

        de_json = ServiceUtil.make_put_request(
            path=DetectionEntity._base_path + str(self.id),
            body=self.to_json()
        )[0]

        detection_entity = DetectionEntity.from_json(de_json)
        self.__dict__ = detection_entity.__dict__
        logging.info(f"Updated DetectionEntity to: \n{self}")

    def get_task_states(self, newer_than: int = 0) -> List[DetectionTaskState]:
        newer_than = guarantee_milliseconds(newer_than)
        dts_jsons = ServiceUtil.make_get_request(
            path=f"detectiontaskstates?detectionEntityId={self.id}&newerThan={newer_than}"
        )
        task_states = [DetectionTaskState.from_json(dts) for dts in dts_jsons]
        task_states.sort(key=lambda s: s._end_detection)
        return task_states

    @staticmethod
    def validate(detection_entity: "DetectionEntity") -> bool:
        is_valid = DetectionEntity.validate_no_timestamp_rename(detection_entity.input_data_config)
        is_valid = is_valid and DetectionEntity.validate_selected_fields(detection_entity.input_data_config)
        return is_valid

    @staticmethod
    def validate_selected_fields(input_data_config: InputDataConfig) -> bool:
        """
        This checks if there are no duplicated input fields.

        :param input_data_config: InputDataConfig to check for duplicate select columns
        :return: True if valid, False if there are duplicate columns selected
        """
        selected_fields = []
        for sc in input_data_config.source_configs:
            sc_selected_fields = set(sc.select)
            for fields in ["timestamp"] + input_data_config.group_by:
                sc_selected_fields.remove(fields)
            for key, value in sc.rename.items():
                sc_selected_fields.remove(key)
                sc_selected_fields.add(value)
            selected_fields += list(sc_selected_fields)
        no_duplicates = len(selected_fields) == len(set(selected_fields))
        if not no_duplicates:
            logging.warning("The provided input data config contains duplicate selected columns. You might want to rename them.")
        return no_duplicates

    @staticmethod
    def validate_no_timestamp_rename(input_data_config: InputDataConfig) -> bool:
        for sc in input_data_config.source_configs:
            for key in sc.rename:
                if key == "timestamp":
                    logging.warning("The provided input data config renames the timestamp column. Please remove this part of the configuration.")
                    return False
        return True

    def to_json(self) -> Any:
        de_json = {
            "projectId": self.project_id,
            "title": self.title,
            "intervalMinutes": self.interval_minutes,
            "inputDataConfig": self.input_data_config.to_json(),
            "inferenceConfig": {
                "modelName": self.inference_model,
                "kubeflowNamespace": self.inference_namespace
            },
            "monitInformation": {
                "producer": self.monit_producer,
                "labels": self.monit_label
            },
            "processingStartTimestamp": self.processing_start_timestamp
        }
        if self.sliding_interval_minutes is not None:
            de_json["slidingIntervalMinutes"] = self.sliding_interval_minutes
        return de_json

    @staticmethod
    def from_json(de_body: Any) -> "DetectionEntity":
        return DetectionEntity(
            detection_entity_id=de_body["id"],
            project=de_body["projectId"],
            title=de_body["title"],
            interval_minutes=de_body["intervalMinutes"],
            sliding_interval_minutes=de_body["slidingIntervalMinutes"] if "slidingIntervalMinutes" in de_body else None,
            input_data_config=InputDataConfig.from_json(de_body["inputDataConfig"]).with_depth(1),
            inference_model=de_body["inferenceConfig"]["modelName"],
            inference_namespace=de_body["inferenceConfig"]["kubeflowNamespace"],
            monit_producer=de_body["monitInformation"]["producer"],
            monit_label=de_body["monitInformation"]["labels"],
            processing_start_timestamp=de_body["processingStartTimestamp"] if "processingStartTimestamp" in de_body else None
        ).with_depth(0)
