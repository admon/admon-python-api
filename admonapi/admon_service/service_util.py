from typing import Any
import os
import requests


class ServiceUtil:
    admon_service_url = os.getenv("ADMON_SERVICE_URL")
    if admon_service_url is None:
        admon_service_url = "http://admon-service.cern.ch/"

    @staticmethod
    def make_get_request(path: str) -> Any:
        res = requests.get(
            ServiceUtil.admon_service_url + path,
            cookies=ServiceUtil.get_auth_cookies()
        )
        if res.status_code != 200:
            raise Exception("Request failed with result: " + str(res.json()))
        return res.json()

    @staticmethod
    def make_post_request(path: str, body: Any = None) -> Any:
        res = requests.post(
            ServiceUtil.admon_service_url + path,
            json=body,
            cookies=ServiceUtil.get_auth_cookies()
        )
        if res.status_code != 200:
            raise Exception("Request failed with result: " + str(res.json()))
        return res.json()

    @staticmethod
    def make_put_request(path: str, body: Any = None) -> Any:
        res = requests.put(
            ServiceUtil.admon_service_url + path,
            json=body,
            cookies=ServiceUtil.get_auth_cookies()
        )
        if res.status_code != 200:
            raise Exception("Request failed with result: " + str(res.json()))
        return res.json()

    @staticmethod
    def make_delete_request(path: str, body: Any = None) -> Any:
        res = requests.delete(
            ServiceUtil.admon_service_url + path,
            json=body,
            cookies=ServiceUtil.get_auth_cookies()
        )
        if res.status_code != 200:
            raise Exception("Request failed with result: " + str(res.json()))
        return res.json()

    @staticmethod
    def get_auth_cookies() -> Any:
        if "JUPYTERHUB_API_TOKEN" in os.environ:
            headers = {"Authorization": "token " + os.getenv("JUPYTERHUB_API_TOKEN")}
            auth_path = os.getenv("JPY_HUB_API_URL") + "/user"
            access_token = requests.get(auth_path, headers=headers).json()["auth_state"]["access_token"]

            return {"user_access_token": access_token}
        else:
            return {"authservice_session": os.getenv("AUTHSERVICE_SESSION")}