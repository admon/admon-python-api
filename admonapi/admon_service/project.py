from typing import List, Any
import logging
from admonapi.admon_service import ServiceUtil
from admonapi.model import ADMONObject


class Project(ADMONObject):
    """
    A descriptive object for a project within the ADMON domain.

    It can be created using various functions:
        .get() # Gives you a single `Project`
        .get_many() # Gives a list of `Project`
        .create() # Creates a new `Project` in the admon-service and gives you the newly created project
    """
    class_name = "Project"
    _base_path = "projects/"

    def __init__(
        self,
        project_id: int,
        title: str,
        project_url: str,
        description: str,
        is_private: bool,
        _depth: int = 0
    ):
        """
        note: A Project should only be created via the `.get()`, `.get_many()` or `create()` methods.
        It should not be directly created via using the constructor.
        """
        super().__init__(_depth)
        self._project_id = project_id
        self._title = title
        self._project_url = project_url
        self._description = description
        self._is_private = is_private

    @property
    def id(self) -> int:
        return self._project_id

    @property
    def title(self) -> str:
        return self._title

    @property
    def project_url(self) -> str:
        return self._project_url

    @property
    def description(self) -> str:
        return self._description

    @property
    def is_private(self) -> bool:
        return self._is_private

    @staticmethod
    def get(title: str = None, project_id: int = None) -> "Project":
        projects = Project.get_many(title, project_id)

        if len(projects) != 1:
            if len(projects) > 1:
                raise Exception("Project with given attributes was ambiguous.")
            else:
                raise Exception("No Project found with given attributes.")
        project = projects[0]
        return project

    @staticmethod
    def get_many(title: str = None, project_id: int = None) -> List["Project"]:
        path = Project._base_path

        if project_id is not None:
            path += "/" + str(project_id)
        if title is not None:
            path += "?title=" + title
        project_jsons = ServiceUtil.make_get_request(path)
        projects = [Project.from_json(project_json) for project_json in project_jsons]

        logging.info(f"Get Projects: \n{projects}")
        return projects

    @staticmethod
    def create(
        title: str,
        project_url: str,
        description: str,
        is_private: bool,
        egroup: str
    ) -> "Project":
        to_be_created_project = Project(
            project_id=0,
            title=title,
            project_url=project_url,
            description=description,
            is_private=is_private
        )
        project_json = ServiceUtil.make_post_request(
            path=Project._base_path,
            body=to_be_created_project.to_json(egroup)
        )[0]

        project = Project.from_json(project_json)
        logging.info(f"Created Project: \n{project}")
        return project

    def update(
        self,
        title: str = None,
        project_url: str = None,
        description: str = None,
        is_private: bool = None
    ) -> "Project":
        to_be_created_project = Project(
            project_id=self.id,
            title=title or self.title,
            project_url=project_url or self.project_url,
            description=description or self.description,
            is_private=is_private or self.is_private
        )
        project_json = ServiceUtil.make_put_request(
            path=Project._base_path + str(self.id),
            body=to_be_created_project.to_json()
        )[0]

        project = Project.from_json(project_json)

        logging.info(f"Updated Project to: \n{project}")
        return project

    def delete(self) -> None:
        ServiceUtil.make_delete_request(
            path=Project._base_path + str(self.id)
        )
        logging.info(f"Deleted Project: \n{self}")

    def to_json(self, egroup: str = None) -> Any:
        return {
            "title": self.title,
            "projectURL": self.project_url,
            "description": self.description,
            "isPrivate": self.is_private,
            "egroup": egroup
        }

    @staticmethod
    def from_json(project_body: Any) -> "Project":
        return Project(
            project_id=project_body["id"],
            project_url=project_body["projectURL"],
            title=project_body["title"],
            description=project_body["description"],
            is_private=project_body["isPrivate"]
        )
