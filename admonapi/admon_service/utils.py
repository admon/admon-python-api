def guarantee_milliseconds(ts: int) -> int:
    ms_timestamp_check = 1E12
    if ts < ms_timestamp_check:
        return ts * 1000
    else:
        return ts

