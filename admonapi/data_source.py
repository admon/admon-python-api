import os
import json
from typing import Union, Tuple
from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.functions import udf, col, explode
from pyspark.sql.types import LongType, DoubleType, StringType, ArrayType, StructType, StructField
from admonapi import SourceConfig, InputDataConfig, DetectionEntity


class DataSource:
    def __init__(self, spark: SparkSession, config: Union[DetectionEntity, SourceConfig, InputDataConfig]):
        self._spark_session = spark
        self.config = config

        if isinstance(self.config, DetectionEntity):
            self.config = self.config.input_data_config

    @staticmethod
    def get_spark() -> SparkSession:
        return SparkSession.builder \
            .master("local[*]") \
            .appName("DataSource") \
            .config("spark.jars", "jars/admon_common.jar") \
            .config("spark.driver.extraJavaOptions",
                    "-Djava.security.auth.login.config=resources/kafkaclient_jaas.conf") \
            .config("spark.executor.extraJavaOptions",
                    "-Djava.security.auth.login.config=resources/kafkaclient_jaas.conf") \
            .getOrCreate()

    def read_kafka(
        self,
        start_time: int,
        end_time: int,
        explode_data: bool = False,
        kafka_input_broker: str = None,
        kafka_password: str = None,
    ) -> DataFrame:
        if not kafka_input_broker:
            kafka_input_broker = os.getenv("KAFKA_INPUT_BROKER")
        if not kafka_password:
            kafka_password = os.getenv("KAFKA_PASSWORD")

        jdf = self._get_source().readKafka(
            start_time,
            end_time,
            kafka_input_broker,
            kafka_password
        )
        return self.add_types(DataFrame(
            jdf=jdf,
            sql_ctx=self._spark_session
        ), explode_data=explode_data)

    def read_hdfs(self, start_time: int, end_time: int, explode_data: bool = False) -> DataFrame:
        jdf = self._get_source().readHDFS(
            start_time,
            end_time
        )
        return self.add_types(DataFrame(
            jdf=jdf,
            sql_ctx=self._spark_session
        ), explode_data=explode_data)

    def _get_source(self) -> "py4j.java_gateway.JavaObject":
        JMonitDataSource = self._spark_session._jvm.ch.cern.admon.api.data.JMonitDataSource

        j_config = self.config.to_java(self._spark_session)
        return JMonitDataSource(self._spark_session._jsparkSession, j_config)

    def add_types(self, data_frame: DataFrame, explode_data: bool = False) -> DataFrame:
        metadata_type, data_type = DataSource.get_types(data_frame)

        def get_metadata(instance_string: str) -> dict:
            instance = json.loads(instance_string)
            return instance["metadata"]
        get_metadata_udf = udf(get_metadata, metadata_type)

        def get_data(instance_string: str) -> list:
            instance = json.loads(instance_string)
            return instance["data"]
        get_data_udf = udf(get_data, data_type)

        data_frame = data_frame.select(
            "Instance",
            get_metadata_udf("Instance").alias("metadata"),
            get_data_udf("Instance").alias("data")
        )

        if explode_data or isinstance(self.config, SourceConfig):
            data_frame = data_frame.select(
                "metadata",
                explode("data").alias("data")
            ).select("metadata.*", "data.*")

        return data_frame

    @staticmethod
    def get_types(data_frame: DataFrame) -> Tuple[StructType, ArrayType]:
        def get_field_type(field):
            if isinstance(field, int):
                return LongType()
            elif isinstance(field, float):
                return DoubleType()
            else:
                return StringType()

        def get_metadata_type(metadata: dict):
            struct_fields = []
            for key in metadata:
                struct_fields.append(StructField(key, get_field_type(metadata[key])))

            return StructType(struct_fields)

        def get_data_type(data: list):
            if data:
                row = data[0]

                struct_fields = []
                for key in row:
                    struct_fields.append(StructField(key, get_field_type(row[key])))

                return ArrayType(StructType(struct_fields))
            else:
                return ArrayType(StructType())

        instance = json.loads(data_frame.limit(1).collect()[0]["Instance"])
        metadata = instance["metadata"]
        data = instance["data"]

        return get_metadata_type(metadata), get_data_type(data)

