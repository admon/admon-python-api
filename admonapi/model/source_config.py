from typing import List, Dict, Any
from admonapi.model import ADMONObject
from pyspark.sql import SparkSession


class SourceConfig(ADMONObject):
    class_name = "Source Config"

    def __init__(
        self,
        producer: str,
        type_prefix: str,
        topic_type: str,
        select: List[str] = None,
        filter_expression: str = "true",
        rename: Dict[str, str] = None,
        _depth: int = 0
    ):
        super().__init__(_depth)
        self.topic_type = topic_type
        self.producer = producer
        self.type_prefix = type_prefix
        self.select = select or ["*"]
        self.filter_expression = filter_expression or ""
        self.rename = rename or {}

    def to_java(self, spark: SparkSession) -> "py4j.java_gateway.JavaObject":
        JSourceConfig = spark._jvm.ch.cern.admon.api.model.JSourceConfig
        JSourceFields = spark._jvm.ch.cern.admon.api.model.JSourceFields
        JTransformConfig = spark._jvm.ch.cern.admon.api.model.JTransformConfig

        return JSourceConfig(
            JSourceFields(self.producer, self.type_prefix, self.topic_type),
            JTransformConfig(self.filter_expression, self.select, self.rename)
        )

    def to_json(self) -> Any:
        transform_config = {}
        if self.filter_expression:
            transform_config["filterExpression"] = self.filter_expression
        if self.select:
            transform_config["selectFields"] = self.select
        if self.rename:
            transform_config["renameMap"] = self.rename

        return {
            "sourceFields": {
                "producer": self.producer,
                "topicType": self.topic_type,
                "typePrefix": self.type_prefix
            },
            "transformConfig": transform_config
        }

    @staticmethod
    def from_json(source_config_body: Any) -> "SourceConfig":
        return SourceConfig(
            topic_type=source_config_body["sourceFields"]["topicType"],
            producer=source_config_body["sourceFields"]["producer"],
            type_prefix=source_config_body["sourceFields"]["typePrefix"],
            select=source_config_body["transformConfig"].get("selectFields", None),
            filter_expression=source_config_body["transformConfig"].get("filterExpression", None),
            rename=source_config_body["transformConfig"].get("renameMap", None),
        )
