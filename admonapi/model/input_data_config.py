from typing import List, Any
from copy import deepcopy
from admonapi.model import ADMONObject, SourceConfig
from pyspark.sql import SparkSession


class InputDataConfig(ADMONObject):
    class_name = "Input Data Config"

    def __init__(
        self,
        source_configs: List[SourceConfig],
        agg_interval_seconds: int = None,
        agg_method: str = None,
        final_filter_expression: str = None,
        final_select: str = None,
        final_rename: str = None,
        group_by: List[str] = None,
        _depth: int = 0
    ):
        super().__init__(_depth)

        if ((agg_interval_seconds is None and agg_method is not None) or
                (agg_interval_seconds is not None and agg_method is None)):
            raise Exception("Invalid Input Data Config. Either define both 'agg' parameters or none.")

        self.source_configs = [sc.with_depth(self._depth + 1) for sc in source_configs]
        self.agg_interval_seconds = agg_interval_seconds
        self.agg_method = agg_method
        self.final_filter_expression = final_filter_expression
        self.final_select = final_select
        self.final_rename = final_rename
        self.group_by = group_by or []

    def to_java(self, spark: SparkSession) -> "py4j.java_gateway.JavaObject":
        JJoinConfig = spark._jvm.ch.cern.admon.api.model.JJoinConfig
        JTransformConfig = spark._jvm.ch.cern.admon.api.model.JTransformConfig
        JInputDataConfig = spark._jvm.ch.cern.admon.api.model.JInputDataConfig

        return JInputDataConfig(
            list([sc.to_java(spark) for sc in self.source_configs]),
            JJoinConfig(self.agg_interval_seconds, self.agg_method),
            JTransformConfig(self.final_filter_expression, self.final_select, self.final_rename),
            self.group_by
        )

    def to_json(self) -> Any:
        join_config = {}
        if self.agg_interval_seconds is not None and self.agg_method is not None:
            join_config = {
                "aggMethod": self.agg_method,
                "aggIntervalSeconds": self.agg_interval_seconds
            }

        final_transform_config = {}
        if self.final_filter_expression is not None:
            final_transform_config["filterExpression"] = self.final_filter_expression
        if self.final_select is not None:
            final_transform_config["selectFields"] = self.final_select
        if self.final_rename is not None:
            final_transform_config["renameMap"] = self.final_rename

        return {
            "sourceConfigs": [sc.to_json() for sc in self.source_configs],
            "groupByFields": self.group_by,
            "joinConfig": join_config,
            "finalTransformConfig": final_transform_config
        }

    @staticmethod
    def from_json(idc_body: Any) -> "InputDataConfig":
        agg_method = None
        agg_interval_seconds = None
        if ("joinConfig" in idc_body and
                "aggMethod" in idc_body["joinConfig"] and
                "aggIntervalSeconds" in idc_body["joinConfig"]):
            agg_method = idc_body["joinConfig"]["aggMethod"]
            agg_interval_seconds = idc_body["joinConfig"]["aggIntervalSeconds"]

        final_filter_expression = None
        final_rename = None
        final_select = None
        if "finalTransformation" in idc_body:
            final_filter_expression = idc_body["finalTransformation"]["filterExpression"]
            final_rename = idc_body["finalTransformation"]["renameMap"]
            final_select = idc_body["finalTransformation"]["selectFields"]

        return InputDataConfig(
            source_configs=[SourceConfig.from_json(sc_body).with_depth(1) for sc_body in
                            idc_body["sourceConfigs"]],
            agg_method=agg_method,
            agg_interval_seconds=agg_interval_seconds,
            final_filter_expression=final_filter_expression,
            final_rename=final_rename,
            final_select=final_select,
            group_by=idc_body["groupByFields"]
        )

    def with_depth(self, depth: int) -> "InputDataConfig":
        self_with_depth = deepcopy(self)
        self_with_depth._depth = depth
        self_with_depth.source_configs = [sc.with_depth(depth + 1) for sc in self.source_configs]
        return self_with_depth
