from dateutil import parser
from datetime import datetime as dt
from typing import Any
from admonapi.model import ADMONObject


class DetectionTaskState(ADMONObject):
    class_name = "Detection Task State"

    def __init__(
        self,
        state: str,
        start_detection: dt,
        end_detection: dt,
        created: dt,
        updated: dt,
        _depth: int = 0
    ):
        super().__init__(_depth)
        self.state = state
        self._start_detection = start_detection
        self._end_detection = end_detection
        self._created = created
        self._updated = updated

    @property
    def start_detection(self) -> str:
        return self._start_detection.strftime('%Y-%m-%d %H:%M:%S')

    @property
    def end_detection(self) -> str:
        return self._end_detection.strftime('%Y-%m-%d %H:%M:%S')

    @property
    def created(self) -> str:
        return self._created.strftime('%Y-%m-%d %H:%M:%S')

    @property
    def updated(self) -> str:
        return self._updated.strftime('%Y-%m-%d %H:%M:%S')

    @staticmethod
    def from_json(detection_task_state_body: Any) -> "DetectionTaskState":
        return DetectionTaskState(
            state=detection_task_state_body["state"],
            start_detection=parser.parse(detection_task_state_body["startDetection"]),
            end_detection=parser.parse(detection_task_state_body["endDetection"]),
            created=parser.parse(detection_task_state_body["created"]),
            updated=parser.parse(detection_task_state_body["updated"])
        )
