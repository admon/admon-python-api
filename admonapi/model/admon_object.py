from typing import List, Any
from copy import deepcopy


class ADMONObject(object):
    class_name = "ADMON Object"

    def __init__(self, _depth: int = 0):
        self._depth = _depth

    def __str__(self) -> str:
        indent = ""
        if "_depth" in dir(self):
            for _ in range(self._depth):
                indent += "│  "

        string_repr = "\n" + indent + f"{self.class_name}\n"
        self_attributes = self._get_attr()
        for attribute in self_attributes[:-1]:
            string_repr += indent + f"├── {attribute}: {getattr(self, attribute)}\n"
        string_repr += indent + f"└── {self_attributes[-1]}: {getattr(self, self_attributes[-1])}"

        return string_repr

    def __repr__(self):
        return self.__str__()

    def _get_attr(self) -> List[str]:
        return [
            attr for attr in dir(self)
            if not attr.startswith('_')
            and not callable(getattr(self, attr))
            and attr not in dir(ADMONObject)
        ]

    def with_depth(self, depth: int) -> Any:
        self_with_depth = deepcopy(self)
        self_with_depth._depth = depth
        return self_with_depth
