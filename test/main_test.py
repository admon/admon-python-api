import unittest
from test.project_test import TestProject
from test.detection_entity_test import TestDetectionEntity
from admonapi import SourceConfig, InputDataConfig, DetectionEntity

if __name__ == '__main__':
    # unittest.main(verbosity=2)

    sc_raw_gled = SourceConfig(
        topic_type="monitoring",
        producer="collectd",
        type_prefix="raw",
        select=["timestamp", "value", "host"],
        filter_expression="topic=='xrootd_raw_gled' AND type_instance=='messages_in' AND plugin=='kafka'",
        rename={"value": "raw_gled_value"}
    )

    sc_raw_alice = SourceConfig(
        topic_type="monitoring",
        producer="collectd",
        type_prefix="raw",
        select=["timestamp", "value", "host"],
        filter_expression="topic=='xrootd_raw_alice' AND type_instance=='messages_in' AND plugin=='kafka'",
        rename={"value": "raw_alice_value"}
    )

    sc_enr_transfer = SourceConfig(
        topic_type="monitoring",
        producer="collectd",
        type_prefix="raw",
        select=["timestamp", "value", "host"],
        filter_expression="topic=='xrootd_enr_transfer' AND type_instance=='messages_in' AND plugin=='kafka'",
        rename={"value": "enr_transfer_value"}
    )

    idc = InputDataConfig(
        source_configs=[sc_raw_gled, sc_raw_alice, sc_enr_transfer],
        agg_interval_seconds=5 * 60,
        agg_method="avg",
        group_by=["host"],
    )

    test_detection_entity = DetectionEntity(
        detection_entity_id=0,
        project=0,
        title="Test",
        interval_minutes=5,
        input_data_config=idc,
        inference_model="",
        inference_namespace="",
        monit_label={},
        monit_producer=""
    )

    print(test_detection_entity)





