import random
import unittest
from admonapi import Project


class TestProject(unittest.TestCase):
    def test_project_lifecycle(self):
        test_title = "Test Project" + str(random.randint(0, 100000))
        test_project_url = "a.test.url"
        test_description = "Test Description"

        project = Project.create(
            title=test_title,
            project_url=test_project_url,
            description=test_description,
            is_private=False,
            egroup="admon-team"
        )

        self.assertEqual(test_title, project.title)
        self.assertEqual(test_project_url, project.project_url)
        self.assertEqual(test_description, project.description)

        updated_title = "Updated Title" + str(random.randint(0, 100000))
        project = project.update(title=updated_title)
        self.assertEqual(updated_title, project.title)

        project = Project.get(updated_title)
        self.assertTrue(isinstance(project, Project))

        projects = Project.get_many()
        is_updated_project_in_list = False
        for p in projects:
            if p.title == updated_title:
                is_updated_project_in_list = True
        self.assertTrue(is_updated_project_in_list)

        project.delete()
        projects = Project.get_many()
        is_updated_project_not_in_list = True
        for p in projects:
            if p.title == updated_title:
                is_updated_project_not_in_list = False
        self.assertTrue(is_updated_project_not_in_list)
