import time
from dotenv import load_dotenv
from pyspark.sql import SparkSession

load_dotenv()
import unittest
from admonapi import SourceConfig, DataSource


class TestDataSource(unittest.TestCase):
    # curl -L -o ./jars/admon_common.jar https://gitlab.cern.ch/admon/admon-common/builds/artifacts/admon112/raw/target/scala-2.12/admon-common-assembly-0.1.jar\?job\=build-uber-jar
    def test_data_source_kafka(self):
        spark = DataSource.get_spark()

        source_config = SourceConfig(
            topic_type="monitoring",
            producer="collectd",
            type_prefix="raw",
            select=['metadata.timestamp', 'value', 'topic', 'plugin', 'type_instance'],
            filter_expression="(topic=='xrootd_raw_gled' OR topic=='xrootd_raw_alice' OR topic=='xrootd_enr_transfer') AND type_instance=='messages_in' AND plugin=='kafka'",
            group_by=["topic"]
        )
        data_source = DataSource(spark, source_config)

        det_end = int(time.time())
        det_start = det_end - 5 * 60

        start_raw = time.time()
        df_raw = data_source.read_kafka(det_start, det_end)
        df_raw.cache()
        print(df_raw.count())
        time_raw = int(time.time() - start_raw)

        start_payload = time.time()
        df_payload = data_source.read_kafka(det_start, det_end, create_payload=True)
        df_payload.cache()
        print(df_payload.count())
        time_payload = int(time.time() - start_payload)

        print(f"Loaded dataframes from kafka:\n  Time taken for raw: {time_raw}s\n  Time taken for payload: {time_payload}s")

    def test_data_source_hdfs(self):
        spark = DataSource.get_spark()

        source_config = SourceConfig(
            topic_type="monitoring",
            producer="collectd",
            type_prefix="raw",
            select=['metadata.timestamp', 'value', 'topic', 'plugin', 'type_instance'],
            filter_expression="(topic=='xrootd_raw_gled' OR topic=='xrootd_raw_alice' OR topic=='xrootd_enr_transfer') AND type_instance=='messages_in' AND plugin=='kafka'",
            group_by=["topic"]
        )
        data_source = DataSource(spark, source_config)

        det_end = int(time.time()) - 60 * 60 * 24 * 5  # five days ago
        det_start = det_end - 5 * 60
        df_hdfs = data_source.read_hdfs(det_start, det_end)
        df_hdfs.show()

