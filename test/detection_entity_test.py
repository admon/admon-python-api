import unittest
import random
from admonapi import DetectionEntity, SourceConfig, Project


class TestDetectionEntity(unittest.TestCase):
    def test_detection_entity_lifecycle(self):
        test_title = "Test Detection Entity" + str(random.randint(0, 100000))
        test_source_config = SourceConfig(
            topic_type="monitoring",
            producer="collectd",
            type_prefix="raw",
            select=["value"]
        )
        test_project = Project.get_many()[0]

        entity = DetectionEntity.create(
            project=test_project,
            title=test_title,
            interval_minutes=5,
            source_config=test_source_config,
            group_by=[],
            inference_model="blank-model",
            inference_namespace="admon-dev",
            monit_producer="admon",
            monit_label={}
        )

        self.assertEqual(test_project.id, entity.project_id)
        self.assertEqual(test_title, entity.title)

        updated_title = "Updated Title" + str(random.randint(0, 100000))
        entity = entity.update(title=updated_title)
        self.assertEqual(updated_title, entity.title)

        entity = DetectionEntity.get(updated_title)
        self.assertEqual(updated_title, entity.title)

        des = DetectionEntity.get_many()
        is_updated_entity_in_list = False
        for e in des:
            if e.title == updated_title:
                is_updated_entity_in_list = True
        self.assertTrue(is_updated_entity_in_list)

        entity.delete()
        des = DetectionEntity.get_many()
        is_updated_entity_not_in_list = True
        for e in des:
            if e.title == updated_title:
                is_updated_entity_not_in_list = False
        self.assertTrue(is_updated_entity_not_in_list)


