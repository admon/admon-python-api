import setuptools

setuptools.setup(
    name="admonapi",
    version="0.1.9",
    author="ADMON dev team",
    author_email="admon@cern.ch",
    description="ADMON python API",
    packages=setuptools.find_packages(),
    url="https://admon-internal.docs.cern.ch/",
    license="MIT License",
    platforms="OS Independent",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=["requests", "pyspark", "python-dateutil"],
)
