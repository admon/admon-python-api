# ADMON Python API

Install with:
```shell
pip install admonapi --extra-index-url https://gitlab.cern.ch/api/v4/projects/132927/packages/pypi/simple 
```

## ADMON python API

### Prerequisites
Have env variables existent in the python environment. It's recommended to use a `.env` file with the `python-dotenv` 
package: 
```python
from dotenv import load_dotenv
load_dotenv("resources/.env")
from admonapi import ...
```
- `AUTHSERVICE_SESSION`: The value of the cookie with the same name from `ml.cern.ch`
- `ADMON_SERVICE_URL`: The url to access the `admon-service`. Currently to be find at: `http://admon-service.cern.ch/`


### Quick Start Guide
The start of using this API is always a `SourceConfig`:
```python
sc = SourceConfig("monitoring", "collectd", "raw")
```
Then we have to create or get a `Project`:
```python
project = Project.create("Test Title", "test.url.ch", "Test description", "your-egroup")
# Or if it already exists in the `admon-service`:
project = Project.get(url="test.url.ch")
```
Both is needed to create a `DetectionEntity`:
```python
de = DetectionEntity.create(
    project=project,
    title="Test Entity",
    interval_minutes=10,
    source_config=sc,
    inference_model="blank-model",
    inference_namespace="admon-dev",
    monit_producer="admon" 
)
# Or if it already exists in the `admon-service`:
de = DetectionEntity.get(id=1)
```

### SourceConfig
Create `SourceConfig` to define which data to get. This is not directly linked by the `admon-service` and only exists 
locally.
```python
sc = SourceConfig(
    topic_type="monitoring",
    producer="collectd",
    type_prefix="raw",
    select=["metadata.timestamp", "value", "topic", "plugin", "type_instance"],
    filter_expression="type_instance='messages_in'",
    rename={"timestamp": "time"}
)
```
You can use this to get data from kafka, hdfs or for a `DetectionEntity`.

### Project
Create `Project` to be able to register a `DetectionEntity` in the next step. The given `egroup` will have 
admin-rights to the created project. 
```python
project = Project.create(
    title="test",
    project_url="test.ch",
    description="Test Project",
    egroup="admon-dev"
)
```

This project is now registered at the `admon-service` and got a `project_id` parameter. You can get it again by
calling:
```python
project2 = Project.get(id=1)
project2 = Project.get("test") # title="test"
```

To get all projects that confirm to a non-unique attribute call:
```python
projects = Project.get_many(description="test")
```

You can update your `Project` at the `admon-service` by calling:
```python
project = project.update(description="New Project Description")
```

### DetectionEntity
To create a `DetectionEntity` you either need a `Project` or know the `project_id`. Here we are using the first project. 
Also, you need a `SourceConfig`. This will create a `DetectionEntity` at the `admon-service`:
```python
de = DetectionEntity.create(
    project=project,
    title="test",
    interval_minutes=10,
    source_config=sc,
    group_by=["plugin", "type_instance"],
    inference_model="blank-model",
    inference_namespace="admon-dev",
    monit_producer="admon"
)
```

Similar to `Project` we can also read `DetectionEntities` from the `admon-service` again.
```python
de2 = DetectionEntity.get("test") # title = test, or id=1
entities = DetectionEntity.get_many(project_id=1)
```

Also, the `DetectionEntity` has an update functionality:
```python
de.update(source_config=SourceConfig(...))
```

### Access Rights
Access rights for projects and their linked detection entities is based on `egroups`.
On creation on `egroup` you are part of has the admin-role.

Role names can be either `admin`, `editor` or `visitor`. Where in non-private projects everyone is considered 
a `visitor` by default.

You can create, get, update and remove `egroup` access rights with the following functions:
```python
project.add_egroup(egroup="new_egroup", role_name="editor")
project.get_egroups()
project.update_egroup(egroup="egroup", new_role_name="visitor")
project.remove_egroup(egroup="egroup")
```

## Getting Data from HDFS or Kafka
Prerequisites:
1. ADMON-Common jar in folder `~/jars`:
```shell
curl -L -o ./jars/admon_common.jar https://gitlab.cern.ch/admon/admon-common/builds/artifacts/admon112/raw/target/scala-2.12/admon-common-assembly-0.1.jar?job=build-uber-jar           
```
2. Resources in folder `~/resources` containing:
   * `.env`: With `KAFKA_PASSWORD=...`
   * `kafkaclient_jaas.conf`
   * `keystore.jks`
   * `truststore.jks`
   * `admonops_auth.keytab` run with `kinit admonops@CERN.CH -k -t "./resources/admonops_auth.keytab"`
3. Running `sshuttle` to access Kafka Data:
   * Install with: `sudo apt update && sudo apt install sshuttle`
   * Run with: `sshuttle --dns -vr <user>@lxtunnel.cern.ch 137.138.0.0/16 128.141.0.0/16 128.142.0.0/16 188.184.0.0/15 --pidfile /tmp/sshuttle.pid --python=python`
   
### Using DataSource
```python
from admonapi import DataSource, SourceConfig

source_config = SourceConfig(
   topic_type = "type",
   producer = "producer",
   type_prefix = "raw",
   select = ["host", "value", "interval"],
   filter_expression = "value > 0",
   group_by = ["host"]
)
data_source = DataSource(spark, source_config)

# DataFrame that contains table of content
df = data_source.read_kafka(start_time, end_time)
# DataFrame with content being transformed to json payload
df = data_source.read_kafka(start_time, end_time, create_payload=True)
```
